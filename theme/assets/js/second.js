var sock_sess = null;

function sock_publish   (topic, args)      { return sock_sess.publish(topic, args); }
function sock_subscribe (topic, handler)   { return sock_sess.subscribe(topic, handler); }

function sock_register  (method, handler)  { return sock_sess.register(method, handler); }
function sock_call      (method, args, cb) { return sock_sess.call(method, args).then(cb); }

$(document).ready(function() {
    var connection = new autobahn.Connection({
       url: "ws://{{ domains }}/junction",
       realm: "octopus"
    });

    connection.onopen = function (session, details) {
        sock_sess = session;

        socks_on_connect(session, details);
    };


    connection.onclose = function (reason, details) {
        sock_sess = null;

        socks_on_connect(reason, details);
    }
});

