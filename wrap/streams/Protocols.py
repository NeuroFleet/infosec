from reactor.shortcuts import *

################################################################################

@Reactor.wamp.register_middleware('fleet.network.Protocols.IPv4')
class IPv4(Reactor.wamp.Nucleon):
    def on_open(self, details):
        pass

    ############################################################################

    @Reactor.wamp.register_topic(u'compute.docker.local.log')
    def error_reporting(self, component, level, message):
        self.invoke(u'logging', 'docker<%s>' % component, level, message)

    #***************************************************************************

    @Reactor.wamp.register_topic(u'compute.docker.local.image')
    def shell_commands(self, args, command, stdout):
        print("[{}] <{}> [STDOUT] {}".format(datetime.now(), 'shell', command, stdout))

    ############################################################################

    @Reactor.wamp.register_method(u'proto.ipv4.ping')
    def ping(self, *images):
        return []

################################################################################

@Reactor.wamp.register_middleware('fleet.network.Protocols.IPv6')
class IPv6(Reactor.wamp.Nucleon):
    def on_open(self, details):
        pass

    ############################################################################

    @Reactor.wamp.register_topic(u'compute.docker.local.log')
    def error_reporting(self, component, level, message):
        self.invoke(u'logging', 'docker<%s>' % component, level, message)

    #***************************************************************************

    @Reactor.wamp.register_topic(u'compute.docker.local.image')
    def shell_commands(self, args, command, stdout):
        print("[{}] <{}> [STDOUT] {}".format(datetime.now(), 'shell', command, stdout))

    ############################################################################

    @Reactor.wamp.register_method(u'proto.ipv6.ping')
    def ping(self, *images):
        return []

