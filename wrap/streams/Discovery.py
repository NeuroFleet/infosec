from reactor.shortcuts import *

################################################################################

@Reactor.wamp.register_middleware('fleet.network.Discovery.DNS')
class DNS(Reactor.wamp.Nucleon):
    def on_open(self, details):
        pass

    ############################################################################

    @Reactor.wamp.register_topic(u'compute.docker.local.log')
    def error_reporting(self, component, level, message):
        self.invoke(u'logging', 'docker<%s>' % component, level, message)

    #***************************************************************************

    @Reactor.wamp.register_topic(u'compute.docker.local.image')
    def shell_commands(self, args, command, stdout):
        print("[{}] <{}> [STDOUT] {}".format(datetime.now(), 'shell', command, stdout))

    ############################################################################

    @Reactor.wamp.register_method(u'<network>.zeroconf.discover')
    def discover(self, *images):
        return []

################################################################################

@Reactor.wamp.register_middleware('fleet.network.Discovery.NetBIOS')
class NetBIOS(Reactor.wamp.Nucleon):
    def on_open(self, details):
        pass

    ############################################################################

    @Reactor.wamp.register_topic(u'compute.docker.local.log')
    def error_reporting(self, component, level, message):
        self.invoke(u'logging', 'docker<%s>' % component, level, message)

    #***************************************************************************

    @Reactor.wamp.register_topic(u'compute.docker.local.image')
    def shell_commands(self, args, command, stdout):
        print("[{}] <{}> [STDOUT] {}".format(datetime.now(), 'shell', command, stdout))

    ############################################################################

    @Reactor.wamp.register_method(u'<network>.zeroconf.discover')
    def discover(self, *images):
        return []

################################################################################

@Reactor.wamp.register_middleware('fleet.network.Discovery.ZeroConf')
class ZeroConf(Reactor.wamp.Nucleon):
    def on_open(self, details):
        pass

    ############################################################################

    @Reactor.wamp.register_topic(u'compute.docker.local.log')
    def error_reporting(self, component, level, message):
        self.invoke(u'logging', 'docker<%s>' % component, level, message)

    #***************************************************************************

    @Reactor.wamp.register_topic(u'compute.docker.local.image')
    def shell_commands(self, args, command, stdout):
        print("[{}] <{}> [STDOUT] {}".format(datetime.now(), 'shell', command, stdout))

    ############################################################################

    @Reactor.wamp.register_method(u'<network>.zeroconf.discover')
    def discover(self, *images):
        return []

