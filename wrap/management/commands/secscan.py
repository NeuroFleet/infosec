from django.core.management.base import BaseCommand, CommandError

from subprocess import Popen

from nucleon.infosec.shortcuts import *

class Command(BaseCommand):
    help = 'Run a modular scanner on target lists.'

    def add_arguments(self, parser):
        parser.add_argument('lists', nargs='+', type=str)

    def handle(self, *args, **options):
        target = ScanHostList.objects.get_or_create(
            narrow = '-'.join(options['lists']),
            module = options['module'],
        )

        target.save()

        lst = []

        for key in options['lists']:
            pth = rpath('profiles','tayamino','infosec','targets','%s.txt' % key)

            if not os.path.exists(pth):
                raise CommandError("Target list '%s' not found at : %s" % (key,pth))

            raw = open(pth).read()

            for ln in raw.split('\n'):
                ln = ln.strip()

                if len(ln):
                    lst.append(ln)

        queue = eventlet.GreenQueue(20)

        print "Using scanner '%s' :" % options['module']

        for dest in lst:
            queue.spawn_n(self.runner, target, options['module'], dest)

        queue.wait()

    def runner(self, lst, mod, dest):
        ret = Reactor.rq.enqueue(scan_addr, lst.id, mod, dest)

        ret.wait()

        print "\t-> Scanned '%s' ..." % dest

