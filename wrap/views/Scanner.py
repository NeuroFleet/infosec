from nucleon.console.shortcuts import *

################################################################################

@login_required
@render_to('infosec/scanner.html')
def homepage(request):
    resp = {
        'stats': [
    dict(color='red',    icon='math',  label="Formulas",     value=0, total=150),
    dict(color='green',  icon='book',  label="Vocabularies", value=0, total=50),
    dict(color='purple', icon='speak', label="Dialects",     value=0, total=100),
    dict(color='blue',   icon='link',  label="References",   value=0, total=20),
        ],
        'frm_scan': ScannerForm()
    }

    if request.method=='POST':
        resp['frm_scan'] = ScannerForm(request.POST)

    return resp

